option('build_overlay',
       type : 'feature',
       description : 'Build overlay')

option('overlay_backends',
       type : 'array',
       value : ['auto'],
       choices : [ 'auto', 'x', 'xv' ],
       description : 'Overlay backends to enable')

option('build_chamelium',
       type : 'feature',
       description : 'Build chamelium test')

option('with_valgrind',
       type : 'feature',
       description : 'Build with support for valgrind annotations')

option('build_man',
       type : 'feature',
       description : 'Build man pages')

option('build_docs',
       type : 'feature',
       description : 'Build documentation')

option('build_tests',
       type : 'feature',
       description : 'Build tests')

option('with_libdrm',
       type : 'array',
       value : ['auto'],
       choices : ['', 'auto', 'intel', 'nouveau', 'amdgpu'],
       description : 'libdrm libraries to be used')

option('with_libunwind',
       type : 'feature',
       description : 'Use libunwind')

option('build_runner',
       type : 'feature',
       description : 'Build test runner')

option('use_rpath',
       type : 'boolean',
       value : false,
       description : 'Set runpath on installed executables for libigt.so')
